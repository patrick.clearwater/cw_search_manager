#!/usr/bin/env python

""" tests for ephemeris.py"""

import pytest
import os
from cw_search_manager import cw_params, ephemeris


@pytest.fixture
def test_par_file():
    return 'tests/test_files/J1801-1417.par'

@pytest.fixture
def test_ephem_file():
    return '/home/meyers/CW/examples/fstat/with_ephemeris/pulsar_ephemerides/O2-pulsar-ephemerides/pulsars.txt'


def test_load_par_file(test_par_file):
    params = ephemeris.PulsarParameters(test_par_file)
    assert(params['BINARY'] is None)

def test_load_pulsars_file(test_ephem_file):
    ephem_list = ephemeris.EphemerisList.from_pulsars_file(test_ephem_file)
    counter = 0
