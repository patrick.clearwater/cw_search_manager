# !/bin/bash

#####################################################
# Some useful ways of running `jstat_viterbi_pipe.py`
# One should not feel like this needs to be edited.
#####################################################



###############################################
# produce fake atoms
###############################################
python ../cw_search_manager/jstat_viterbi_pipe.py --freq-start 20.1 \
                             --config-file test_jstat_pipe_mfdv5.ini \
                             --top-level-directory 'test/' \
                             --clean-sfts \
                             --make-fake-data



###############################################
# produce atoms from real data with cache files
# produce cache files
# to be run on CIT
###############################################
# python jstat_viterbi_pipe.py --freqStart 20.1 \
#                              --config-file test_jstat_pipe.ini \
#                              --top-level-directory 'test/' \
#                              --cache-files '../test_files/l1.cache,../test_files/h1.cache' \
#                              --produce-atoms

###############################################
# run viterbi from cjs
###############################################
# echo 'running cpu code'
# python ../cw_search_manager/jstat_viterbi_pipe.py --freqStart 20.1 \
#                              --config-file test_jstat_pipe.ini \
#                              --top-level-directory 'test/' \
#                              --orbitTp 1238166353 \
#                              --asini 2.35 \
#                              --run-viterbi-cpu
# 
# echo 'running gpu code'
# ###############################################
# # Run GPU code, but you need to supply the gpu executable
# # if you want to do this.
# ###############################################
# python ../cw_search_manager/jstat_viterbi_pipe.py --freqStart 20.1 \
#                              --config-file test_jstat_pipe.ini \
#                              --top-level-directory 'test/' \
#                              --orbitTp 1238166343 \
#                              --orbitTp-step 1\
#                              --orbitTp-end 1238166363 \
#                              --asini 2.35 \
#                              --asini-step 0.01 \
#                              --asini-end 2.45 \
#                              --gpu-executable './viterbi_jstat_search' \
#                              --run-viterbi-gpu
# 
