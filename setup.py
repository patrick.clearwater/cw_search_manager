#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Pat Meyers",
    author_email='pat.meyers@unimelb.edu.au',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="A simple search manager for CW searches...tailored mainly for LMXB Viterbi searches.",
    entry_points={
        'console_scripts': [
            'cw_search_manager=cw_search_manager.cli:main',
        ],
    },
    scripts = ['cw_search_manager/jstat_viterbi_pipe.py','cw_search_manager/viterbi_pipe.py'],
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='cw_search_manager',
    name='cw_search_manager',
    packages=find_packages(include=['cw_search_manager']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/meyers-academic/cw_search_manager',
    version='0.1.0',
    zip_safe=False,
)
